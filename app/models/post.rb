class Post < ApplicationRecord
  belongs_to :user
  validates :contents, length: {maximum: 280}
end
